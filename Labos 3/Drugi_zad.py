# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 19:55:24 2016

@author: hrvoj
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')

carscil = mtcars.groupby('cyl')
carscilAVG = carscil.sum()/carscil.count()
carscilAVG.mpg.plot(kind='bar')
plt.show()

carscilAVG.wt.plot(kind='box')
plt.show()

novicars = mtcars.groupby('am')
AVG = novicars.sum()/novicars.count()
AVG.mpg.plot(kind='bar')
plt.show()