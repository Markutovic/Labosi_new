# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')

consuption = mtcars.sort(['mpg'])
print consuption[-5:]

cil8 = mtcars[mtcars.cyl==8]
cil8s = cil8.sort(['mpg'])
print cil8s.head(3)

cil6 = mtcars[mtcars.cyl==6]
cil6_AVG = sum(cil6.mpg)/len(cil6)
print cil6_AVG

cil4 = mtcars[mtcars.cyl==4]
cil4_LBS = cil4[(cil4.wt>=2.0) & (cil4.wt<=2.2)]
cil4_AVG = sum(cil4_LBS.mpg)/len(cil4_LBS)
print cil4_AVG

auto = mtcars[mtcars.am==1]
manual = mtcars[mtcars.am==0]
br_auto=len(auto)
br_manual = len(manual)
print ('Automatski: '), br_auto
print ('Rucni: '), br_manual

autovis = auto[auto.hp>100]
num_autovis = len(autovis)
print ('Automatski preko 100 KS: '), num_autovis

LBS_into_KG=mtcars.wt*1000*0.45
print LBS_into_KG