﻿# -*- coding: utf-8 -*-
"""
Created on Dec 01 14:21:48 2016

@author: hrvoje
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
def non_func(x): #definiranje funkcije za dobivanje mjerenih vrijednosti
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y
def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
x = np.linspace(1,10,100) #postavljanje broja uzoraka
y_true = non_func(x)
y_measured = add_noise(y_true)
plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)
np.random.seed(12)
indeksi = np.random.permutation(len(x)) #dodavanje slučajno generirane vrijednosti
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))] #podaci za ucenje
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)] #podaci za testiranje
x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]
xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]
xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]
plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)
linearModel = lm.LinearRegression() #definiranje linearne regresije
linearModel.fit(xtrain,ytrain)
print 'Model je oblika y_hat = Theta0 + Theta1 * x'
print 'y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x'
ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)
plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)
x_pravac = np.array([1,10]) #pravac koji bi trebao odvajati podatke
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)

#Drugi zadatak: odrediti parametre lin. modela na podacima za ucenje
#na nacin da se implementira funkcija za izracunavanje parametara lin morela iz prvog zadatka

x_ones = np.ones(len(xtrain))
x_ones = x_ones[:, np.newaxis]
X = np.column_stack((x_ones, xtrain))
print X
def theta_ML(x, ytrain): #definicija funkcije koja izracuna koeficijent pravca
    return np.dot(inv(np.dot(X.T,X)), (np.dot(X.T,ytrain)))
print theta_ML(X,ytrain)

#Treci zadatak - na podacima iz prvog zadatka odrediti parametre lin. modela 
#na podacima za ucenje na nacin da se implementira funkcija za izracunavanje parametara lin. modela metodom gradijentalnog spusta.
#Usporediti dobivene parametre modela s vrijednostima parametara lin. modela iz prvog zadatka.
theta_p=np.array([[0.0, 0.0]]).T
theta = np.zeros((2,1))
n=len(X)
alpha=0.05
br_iter=100
J=np.zeros(br_iter)
def gradSpust(x, y, theta_p, alpha, br_iter):
    for i in range(0, br_iter):
        h=np.dot(X,theta_p)
        e=h-y
        J[i]=np.sum(np.power(e,2))/n
        grad0=np.dot(e.T, X[:,0])
        grad0=grad0/n
        grad1=np.dot(e.T, X[:,1])
        grad1=grad1/n
        theta[0]=theta_p[0]-alpha*grad0
        theta[1]=theta_p[1]-alpha*grad1
        theta_p=theta
        
gradSpust(X,ytrain,theta_p,alpha,br_iter)
plt.figure()
plt.plot(range(0,br_iter),J)
plt.ylabel('J')
plt.xlabel('Broj iteracija')

#Cetvrti zadatak - napisati funkciju za racunanje srednje kvadratne pogreske ikoeficijenda determinacije.
#Koliko iznose ove mjere na skupu za ucenje, a koliko na skupu za testiranje? Usporediti dobivene vrijednosto s vrijednostima
#koj evracaju odgovarajuce funkcije iz biblioteke

y_train_p=linearModel.predict(xtrain)
y_test_p=linearModel.predict(xtest)
def MSE(ytrain, y_train_p):
    MSEtrain=np.sum(np.power((ytrain-y_train_p),2))/len(ytrain)
    MSEtest=np.sum(np.power((ytest-y_test_p), 2))/len(ytest)
    
    print MSEtrain
    print MSEtest
    
MSE(ytrain, y_train_p)
MSEtrain_=mean_squared_error(ytrain, y_train_p)
MSEtest_=mean_squared_error(ytest, y_test_p)
print MSEtrain_
print MSEtest_

xte_one=np.ones(len(xtest))
xte_one=xte_one[:, np.newaxis]
Xte=np.column_stack((xte_one,xtest))
THETA=np.array([[0.14219055, 0.3225882]]).T
H=np.dot(X, THETA)
Hte=np.dot(Xte, THETA)

def r2_score(ytrain, ytest, H, Hte):
    r2_train=np.sum(1-((np.sum(np.power(ytrain-H,2)))/(np.sum(ytrain-ytrain/len(ytrain)))))
    r2_test=1-((np.sum(np.power(ytest-Hte,2)))/(np.sum(ytest-ytest/len(ytest))))
    print r2_train
    print r2_test
    
r2_score(ytrain,ytest, H, Hte)
r2train=r2_score(ytrain,y_train_p)
r2test=r2_score(ytest,y_test_p)
print r2train
print r2test

#peti zadatak - prikazati reziduale
etr=ytrain-H
ete=ytest-Hte
print etr
print ete

#sesti zadatak
