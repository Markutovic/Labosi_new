# -*- coding: utf-8 -*-
"""
Created on Sat Dec 17 19:24:55 2016

@author: hrvoje
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as mat

def generate_data(n):
    
    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]

    return data
    
#Prvi zadatak - Generirati skup za ucenje i testiranje
    
np.random.seed(242)
data_train = generate_data(200)
np.random.seed(242)
data_test = generate_data(100)

#Drugi zadatak - Prikazati generirane podatke te razlikovati

plt.scatter(data_train[:,0], data_train[:,1], c=data_train[:,2], cmap='Purples')

#Treci zadatak - Izgraditi model logisticke regresije

Log_Reg_Model = lm.LogisticRegression()
Log_Reg_Model.fit(data_train[:,0:2], data_train[:,2]) #odredivanje parametara fit metodom
Th0 = Log_Reg_Model.intercept_
Th1 = Log_Reg_Model.coef_

x2=(-Th0-Th1[:,0]*data_train[:,0])/Th1[:,1]
x2=x2[:,np.newaxis]
plt.scatter(data_train[:,0],data_train[:,1], c=data_train[:,2], cmap='Purples')
plt.plot(data_train[:,0],x2)  #prikaz granice odluke

#Peti zadatak - Provesti klasifikaciju testnog skupa podataka

ytest=Log_Reg_Model.predict(data_test[:,0:2]) #dobivanje izlaza pomoću naredbe za predikciju
ytest=ytest[:, np.newaxis]

xtest=data_test[:,2] #određivanje prva dva stupca
xtest=xtest[:, np.newaxis]
y=ytest-xtest

#Cetvrti zadatak - Prikaz logisticke regresije u obliku vjerojatnosti zajedno s podacima za ucenje

f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(data_train[:,0])-0.5:max(data_train[:,0])+0.5:.05,
min(data_train[:,1])-0.5:max(data_train[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = Log_Reg_Model.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logističke regresije')
plt.show()

#sesti zadatak - prikazati matricu zabune

def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
        
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)
            
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()
    
conf_mat=mat.confusion_matrix(xtest,ytest)
plot_confusion_matrix(conf_mat)

#ostali zadaci

import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing

def plot_KNN(KNN_model, X, y):
    
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x1_min, x1_max, 0.01),
                         np.arange(x2_min, x2_max, 0.01))
    Z1 = KNN_model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z1.reshape(xx.shape)
    plt.figure()
    plt.pcolormesh(xx, yy, Z, cmap='PiYG', vmin = -2, vmax = 2)
    plt.scatter(X[:,0], X[:,1], c = y, s = 30, marker= 'o' , cmap='RdBu',
    edgecolor='white', label = 'train')
    
# klasifikacija rukom pisanih brojeva
# ulazni podaci su slike rezolucije 8x8; ukupno je na raspolaganju 1797 slika
# svaka slika pripada odgovarajućoj klasi (brojevi od 0 do 9)
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.datasets import load_digits
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.cross_validation import train_test_split
# ucitaj podatke
digits = load_digits()
# prikaži osnovne informacije o skupu podataka
print(digits.DESCR)
# ucitaj ulazne velicine (slike) u matricu X dimenzije 1797 sa 64
# (svaka 8x8 slika je razvucen u polje duzine 64 gdje je element vektora jedan pixel na slici)
# odogvarajuca klasa svake slike nalazi se u polju y (dimenzije 1797 x 1)
X, y, images = digits.data, digits.target, digits.images
# 80% slika uzmi za učenje modela, 20% za testiranje
X_train, X_test, y_train, y_test, images_train, images_test = train_test_split(
                                X, y, images, test_size=0.20, random_state=67)
# izgradi model logističke regresije na podacima za učenje
LogRegModel = LogisticRegression()
LogRegModel.fit(X_train, y_train)
# pomoću modela procijeni klasu svake slike iz testnog skupa
y_test_predict = LogRegModel.predict(X_test)
# usporedi procjenu za svaku sliku testnog skupa sa stvarnom klasom
print("Vrjednovanje klasifikatora %s:\n%s\n"
      % (LogRegModel, classification_report(y_test, y_test_predict)))
print("Matrica zabune:\n%s" % confusion_matrix(y_test, y_test_predict))
# prikaži četiri slike testnog skupa i procjenu klasifikatora
fig = plt.figure()
n_samples = len(digits.images)
images_and_predictions = list(zip(images_test, y_test_predict))
for index, (image, prediction) in enumerate(images_and_predictions[:4]):
    plt.subplot(2, 4, index + 5)
    plt.axis('off')
    plt.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
    plt.title('Prediction: %i' % prediction)
plt.show()