# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 18:55:38 2016

@author: hrvoje
"""

#treci zadatak

import numpy as np
import matplotlib.pyplot as plt

b = np.random.random_integers(1, 6, 100)

print (b)

plt.hist(b, bins=[0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5])