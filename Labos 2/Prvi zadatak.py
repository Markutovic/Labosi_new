# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 21:28:48 2016

@author: hrvoje
"""
import re

ime=input("Ime: ")
try:
    fhand=open(ime)           
except:
    print ("Datoteke nema", ime)
    exit()
    
for line in fhand:
    line=line.rstrip()
    username=re.findall('([\w\.-]+)@[\w\.-]+', line)
    if (len(username)>0):
        print (username)