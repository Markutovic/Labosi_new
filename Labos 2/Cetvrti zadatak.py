# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 18:58:21 2016

@author: hrvoje
"""

import numpy as np
import matplotlib.pyplot as plt
average = np.zeros(1000)
dev=np.zeros(1000)

for i in range(0,1000):
    br=np.random.random_integers(1, 6, 30)
    x=sum(br)
    average[i]=x/30
    dev[i]=np.std(br)
plt.hist(average)
